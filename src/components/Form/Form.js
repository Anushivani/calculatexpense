import {useState} from "react";
import Display from "../Display/Display";
import styles from "./Form.module.css";
const Form = () => {
  const [btnClick, setBtnClick] = useState();
  

  const [inputData, setInputData] = useState({
    id: "",
    title: "",
    amount: "",
    date: "",
  });
  const [multipleData, setMultipleData] = useState([]);
  let [userEnteredTotalAmount,setUserEnteredTotalAmount]=useState(0);
  const handleChangeTitle = (e) => {
    setInputData({...inputData, title: e.target.value});
  };
  const handleChangeAmount = (e) => {
    setInputData({...inputData, amount: e.target.value});
  };
  const handleChangeDate = (e) => {
    setInputData({...inputData, date: e.target.value});
  };
  const handleSubmitForm = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setBtnClick("btn was clicked");
    console.log(btnClick);
    console.log(inputData, "inputData");
    setMultipleData([...multipleData, inputData]);
    const val=userEnteredTotalAmount+parseInt(inputData.amount);
    setUserEnteredTotalAmount(val);
    console.log(userEnteredTotalAmount);
  };
  console.log("multipleData", multipleData);
  return (
    <div className={styles.formdata}>
      <form onSubmit={handleSubmitForm}>
        <div className={styles.title}>
          <input
            type="text"
            placeholder="enter title"
            value={inputData.title}
            onChange={handleChangeTitle}
          />
        </div>
        <div>
          <input
            type="number"
            placeholder="enter amount"
            value={inputData.amount}
            onChange={handleChangeAmount}
          />
        </div>
        <div>
          <input
            type="date"
            placeholder="enter date"
            value={inputData.date}
            onChange={handleChangeDate}
          />
        </div>
        <button type="submit">ADD</button>
      </form>
      <Display
        inputData={inputData}
        setInputData={setInputData}
        multipleData={multipleData}
        userEnteredTotalAmount={userEnteredTotalAmount}

      />
    </div>
  );
};
export default Form;
